export type PaginationPropsType = {
    total: number
    showSizeChanger?: boolean
    pageSize: number
    current: number
    pageSizeOptions?: number[] | string[],
    onChange?: (page: number, pageSize: number) => void
}
