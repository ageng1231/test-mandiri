import { NzTableFilterList, NzTableSortFn } from "ng-zorro-antd/table"

export type ColumnPropsType = {
    key: string
    label: string
    sortFn: boolean | NzTableSortFn | null,
    colspan?: string,
    rowspan?: string
}
