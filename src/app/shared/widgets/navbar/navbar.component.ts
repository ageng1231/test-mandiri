import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppImage } from '@mandiri/config/app-image';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzFlexModule } from 'ng-zorro-antd/flex';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzTypographyModule } from 'ng-zorro-antd/typography';

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [
    NzMenuModule,
    CommonModule,
    NzImageModule,
    NzTypographyModule,
    NzAvatarModule,
    NzFlexModule,
    NzButtonModule
  ],
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.css'
})
export class NavbarComponent {
    constructor(
      public appImage: AppImage,
      private route: Router
    ){}
    public logout(){
      this.route.navigateByUrl('')
    }
    public goTo(props: string){
        this.route.navigateByUrl(props)
    }
}
