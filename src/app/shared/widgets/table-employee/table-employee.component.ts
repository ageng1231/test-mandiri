import { Component } from '@angular/core';
import { TableEmployeeModel } from './table-employee-model.model';
import { EmployeeEntity } from '@mandiri/infrastructures/core/entities/employee-entity';
import { CommonModule } from '@angular/common';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzInputModule } from 'ng-zorro-antd/input';
import { Router } from '@angular/router';
import { AppConfig } from '@mandiri/config/app-config';
import { SupabaseClient } from '@supabase/supabase-js';
import { EmployeeServiceService } from '@mandiri/infrastructures/domain/services/employee-service.service';
import {faker} from '@faker-js/faker';
import { ObservedValueOf, timeout } from 'rxjs';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-table-employee',
  standalone: true,
  imports: [
    CommonModule,
    NzTableModule,
    NzButtonModule,
    NzGridModule,
    NzInputModule,
    FormsModule
  ],
  templateUrl: './table-employee.component.html',
  styleUrl: './table-employee.component.css'
})
export class TableEmployeeComponent {
    state: TableEmployeeModel = new TableEmployeeModel();
    supabase: SupabaseClient;
    constructor(
       private router: Router,
       private service: EmployeeServiceService
    ){
      const appconfig = new AppConfig();
      this.supabase = appconfig.getClient();
    }
    ngOnInit(){
      let row: EmployeeEntity = {
          employee_id: 'id-1',
          username: 'employee_1',
          firstName: 'firstname',
          lastName: 'lastname',
          email: 'email@email.com',
          birthDate: new Date('1980-01-01'),
          basicSalary: 10000000,
          status: 'active',
          group: 'staff',
          description: 'desc'
      }
      this.state.data.push(row)
      this.fetchData();
    }
    async fetchData(){
      console.warn(this.supabase.from('*'))
      const jsonData = await this.service.getAllEmployeeData({
        page: this.state.pagination.current,
        limit: this.state.pagination.pageSize,
        query: this.state.search,
        sort: this.state.sort as never[]
      });
      console.warn(jsonData)
      this.state.data = jsonData.data
      this.state.pagination.total = jsonData.count
    } 
    
    public onPageIndexChange(props: any){
      this.state.pagination.current = props;
      this.fetchData()
    }
    public onPageSizeChange(props: any){
      console.warn(props);
    }
    timeoutId?: any;
    public searchBarHandler(){
      console.warn('searchbar trigger')
      clearTimeout(this.timeoutId);
      this.timeoutId = setTimeout(() => {
          this.fetchData();
        }, 1000)
        

    }
    sortFn = (a: any, b: any) => {
        let row: any;
        console.warn(a)
        if(a == 'ascend'){
          row = {column: b, order: 'asc'};
        }
        else if(a =='descend'){
          row = {column: b, order: 'desc'};
        }else{
          row = null
        }
        let sortDataState = this.state.sort;
        console.warn('sortds', sortDataState)
        let dataIndex = this.state.sort.findIndex(x => x.column == b);
        console.warn(dataIndex)
        if(dataIndex >= 0 && row != null){
            this.state.sort[dataIndex] = row
        }else if(dataIndex < 0 && row != null){
          this.state.sort.push(row)
        }else if(dataIndex > 0 && row == null){
          delete this.state.sort[dataIndex]
        }else{

        }
        console.warn(this.state.sort)
        clearTimeout(this.timeoutId);
        this.timeoutId = setTimeout(() => {
            this.fetchData();
          }, 1000)            
    }

    public onAddHandler(){
        this.router.navigateByUrl('/employee/form')
    }
    public onViewHandler(props: any){
      console.warn(props)
      this.router.navigateByUrl('/employee/view/' + props, {state: {id: props, editable: false}})
    }
    public onEditHandler(props: any){
      this.router.navigateByUrl('/employee/form/' + props, {state: {id: props, editable: true}})

    }
    public onDeleteHandler(props: any){

    }
}
