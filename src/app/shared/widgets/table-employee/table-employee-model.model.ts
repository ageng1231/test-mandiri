import { EmployeeEntity } from "@mandiri/infrastructures/core/entities/employee-entity";
import { PaginationPropsType } from "@mandiri/shared/entities/pagination-props-type.type";

export class TableEmployeeModel {
    data: EmployeeEntity[] = []
    pagination: PaginationPropsType = {
       current: 1,
       pageSize: 10,
       pageSizeOptions: [10,25,50,100],
       total: 0
    }
    search: string = ''
    sort: any[] = []
}
