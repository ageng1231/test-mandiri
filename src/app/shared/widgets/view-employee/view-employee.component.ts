import { CommonModule, Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { EmployeeEntity } from '@mandiri/infrastructures/core/entities/employee-entity';
import { EmployeeServiceService } from '@mandiri/infrastructures/domain/services/employee-service.service';
import { CurrencyUtils } from '@mandiri/shared/utils/currency-utils';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzFlexModule } from 'ng-zorro-antd/flex';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzTypographyModule } from 'ng-zorro-antd/typography';

@Component({
  selector: 'mandiri-view-employee',
  standalone: true,
  imports: [
    NzFlexModule,
    NzTypographyModule,
    CommonModule,
    NzGridModule,
    NzButtonModule
  ],
  templateUrl: './view-employee.component.html',
  styleUrl: './view-employee.component.css'
})
export class ViewEmployeeComponent implements OnInit{
    state: EmployeeEntity
    constructor(
      private service: EmployeeServiceService,
      public currency: CurrencyUtils,
      private location: Location
    ){

    }
    ngOnInit(): void {
        let props: any = window.history.state;
        this.initData(props)
    }
    async initData(props: any){
      let json = await this.service.getData(props.id);
      this.state = json
    }
    parseCurrency(props: string): string {
      return this.currency.formatNumber(parseFloat(props));
    }
    public cancelButtonHandler(){
      this.location.back();
    }
}
