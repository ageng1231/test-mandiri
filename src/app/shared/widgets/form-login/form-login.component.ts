import { Component, OnInit } from '@angular/core';
import { AppImage } from '@mandiri/config/app-image';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzFlexModule } from 'ng-zorro-antd/flex';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzInputModule } from 'ng-zorro-antd/input';
import { FormLoginModel } from './form-login-model';
import { NonNullableFormBuilder, ReactiveFormsModule } from '@angular/forms';
import { AuthServiceService } from '@mandiri/infrastructures/domain/services/auth-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form-login',
  standalone: true,
  imports: [
    NzFormModule,
    NzInputModule,
    NzIconModule,
    NzButtonModule,
    NzImageModule,
    NzFlexModule,
    ReactiveFormsModule
  ],
  templateUrl: './form-login.component.html',
  styleUrl: './form-login.component.css'
})
export class FormLoginComponent implements OnInit{
  state: FormLoginModel = new FormLoginModel();
    constructor(
      public appImage: AppImage,
      private fb: NonNullableFormBuilder,
      private service: AuthServiceService,
      private route: Router
    ){}
    ngOnInit(){
      this.state.authenticating = false;
      this.initFormGroup();
    }
    initFormGroup(){
      this.state.formLogin = this.fb.group({
        username: [''],
        password: ['']
      }) as unknown as typeof this.state.formLogin
    }
    public async submitHandler(){
      this.state.authenticating = true;
      let data = this.state.formLogin.value;
      let authenticate = await this.service.login(data);
      if(authenticate.data != null){
        this.route.navigate(['/employee']);
      }
      console.warn(authenticate);
      this.state.authenticating = false;
    }
    public toggleVisiblePassword(){
      this.state.passwordVisible = !this.state.passwordVisible
    }
}
