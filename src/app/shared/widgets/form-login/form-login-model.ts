import { Form, FormControl, FormGroup } from "@angular/forms";

export class FormLoginModel {
    authenticating: boolean = false;
    passwordVisible: boolean = false;
    formLogin: FormGroup<{
        email: FormControl<string>
        password: FormControl<string>
    }>
}
