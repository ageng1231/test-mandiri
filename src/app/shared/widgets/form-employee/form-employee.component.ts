import { Component } from '@angular/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { FormEmployeeModel } from './form-employee.model';
import { FormControl, NonNullableFormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { CommonModule, Location } from '@angular/common';
import { AppValidators } from '@mandiri/config/validators';
import { ValidationErrorsType } from '@mandiri/config/validator.type';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { EmployeeServiceService } from '@mandiri/infrastructures/domain/services/employee-service.service';
import { EmployeeEntity } from '@mandiri/infrastructures/core/entities/employee-entity';
import { Router } from '@angular/router';

@Component({
  selector: 'mandiri-form-employee',
  standalone: true,
  imports: [
    NzFormModule, ReactiveFormsModule, CommonModule,
    NzInputModule, NzButtonModule, NzSelectModule, NzAlertModule,
    NzDatePickerModule
  ],
  templateUrl: './form-employee.component.html',
  styleUrl: './form-employee.component.css'
})
export class FormEmployeeComponent {
    state: FormEmployeeModel = new FormEmployeeModel()
    routeProps: any;
    constructor(
      private fb: NonNullableFormBuilder,
      private service: EmployeeServiceService,
      private route: Router,
      private _location: Location
    ){
      this.state.formEmployeeGroup = this.fb.group({
        username: ['',[AppValidators.requiredField('Username')]],
        firstName: ['', [AppValidators.requiredField('First Name'), AppValidators.noNumber]],
        lastName: ['', [AppValidators.requiredField('Last Name'),AppValidators.noNumber]],
        email: ['', [AppValidators.requiredField('Email'),AppValidators.email]],
        status: ['', [AppValidators.requiredField('Status')]],
        group: ['', [AppValidators.requiredField('Group')]],
        birthDate: ['', [AppValidators.requiredField('Birth Date')]],
        basicSalary: ['',[AppValidators.requiredField('Salary'),AppValidators.numberOnly]],
        deskripsi: [''],
      }) as unknown as typeof this.state.formEmployeeGroup;
    }
    async validateForm(){ 
      let isValidate = this.state.formEmployeeGroup.valid;
      
      if(isValidate){
        console.warn(this.state.formEmployeeGroup.value);
        let save: any;
        if(this.routeProps.id){
          console.warn('update')
          save = await this.service.updateData(this.routeProps.id,this.state.formEmployeeGroup.value as unknown as EmployeeEntity)
        }else{
          save = await this.service.insertData(this.state.formEmployeeGroup.value as unknown as EmployeeEntity)

        }
        console.warn(save)
        if(save.status == 201 || save.status == 204){
          this.route.navigateByUrl('employee')
        }
      }else{
        console.warn(this.state.formEmployeeGroup.value)
        Object.values(this.state.formEmployeeGroup.controls).forEach(control => {
          if(control.invalid){
            control.markAsDirty()
            control.updateValueAndValidity({onlySelf: true})
          }
        })
      }
      
    }
    ngOnInit(){
      let props = window.history.state
      this.state.editable = true
      if(props.id != ''){
        this.routeProps = props;
        if(props.editable == false){
          Object.values(this.state.formEmployeeGroup.controls).forEach(control => {
            control.disable()
          })
          this.state.editable = false
        }
        this.fetchData(props.id)
      }else[
        this.routeProps = {
          id: undefined
        }
      ]
    }
    async fetchData(id: string){
      let jsonData: EmployeeEntity = await this.service.getData(id)
      delete jsonData.employee_id;
      this.state.formEmployeeGroup.setValue({
        firstName: jsonData.firstName,
        lastName: jsonData.lastName,
        email: jsonData.email,
        status: jsonData.status,
        group: jsonData.group,
        username: jsonData.username,
        basicSalary: jsonData.basicSalary.toString(),
        deskripsi: jsonData.description,
        birthDate: jsonData.birthDate.toString(),
      })
    }
    printMessage(props: any): string{
      return props.message;
    }
    btnCancelHandler(): void {
        this._location.back();
    }
}
