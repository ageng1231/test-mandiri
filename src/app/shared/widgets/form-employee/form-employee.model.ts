import { FormControl, FormGroup } from "@angular/forms";

export class FormEmployeeModel {
    statusList: any[] = [];
    groupList: any[] = [];
    editable: boolean = true;
    formEmployeeGroup: FormGroup<{
        username: FormControl<string>;
        firstName: FormControl<string>;
        lastName: FormControl<string>;
        email: FormControl<string>;
        status: FormControl<string>;
        group: FormControl<string>;
        birthDate: FormControl<string>;
        basicSalary: FormControl<string>;
        deskripsi: FormControl<string>;
    }>;
}
