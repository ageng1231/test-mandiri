import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})
export class CurrencyUtils {
    public formatNumber(value: number): string{
        let output = 'Rp. 0';
        let formatter = new Intl.NumberFormat('id-ID', {
            style: 'currency',
            currency: 'IDR',
            minimumFractionDigits: 0
        })
        output = formatter.format(value)
        return output
    }
}
