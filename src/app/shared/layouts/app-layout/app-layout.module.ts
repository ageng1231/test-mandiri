import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppLayoutComponent } from './app-layout.component';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NavbarComponent } from '@mandiri/shared/widgets/navbar/navbar.component';



@NgModule({
  declarations: [
    AppLayoutComponent
  ],
  exports: [
    AppLayoutComponent
  ],
  imports: [
    CommonModule, NzLayoutModule, NavbarComponent
  ],
})
export class AppLayoutModule { }
