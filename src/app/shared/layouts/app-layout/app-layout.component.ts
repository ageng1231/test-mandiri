import { Component } from '@angular/core';

@Component({
  selector: 'logged-in-layout',
  standalone: false,
  templateUrl: './app-layout.component.html',
  styleUrl: './app-layout.component.css',
  
})
export class AppLayoutComponent {

}
