import { Injectable } from '@angular/core';
import {SupabaseClient, createClient } from '@supabase/supabase-js'
@Injectable({
    providedIn: 'root'
})
export class AppConfig {
    private supabaseClient: SupabaseClient
    constructor(){
        this.dbase();
    }
    public dbase (){
        this.supabaseClient =createClient(
            'https://ascxryvuzhiuyoxspcex.supabase.co',
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImFzY3hyeXZ1emhpdXlveHNwY2V4Iiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTkxMTQ4OTksImV4cCI6MjAzNDY5MDg5OX0.FUrYjER5TtpGVF4gIjqnEuflIJsMuDyqtNi5pYb3yZM'
        )
    }
    public getClient(){
        return this.supabaseClient;
    }
}
