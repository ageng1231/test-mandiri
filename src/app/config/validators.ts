import { AbstractControl, ValidatorFn, Validators } from "@angular/forms";
import { ValidationErrorsType } from "./validator.type";
const hasUpperCase: RegExp = /[A-Z]+/;
const hasLowerCase: RegExp = /[a-z]+/;
const hasNumber: RegExp = /[0-9]+/;
const hasSpecialCharacter: RegExp = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;

export class AppValidators extends Validators {
    
    static override email(control: AbstractControl): ValidationErrorsType | null {
        if(Validators.email(control) == null){
            return null;
        }
        return {email: {message: 'Format Email Tidak Valid'}};
    }
    static override minLength(minLength: number): ValidatorFn {
        return (control: AbstractControl): ValidationErrorsType | null => {
            if(Validators.minLength(minLength)(control) === null){
                return null
            }
            return {minlength: {message: 'Minimal Karakter Harus lebih dari ' + minLength + " Karakter"}}
        }
    }
    static override maxLength(maxLength: number): ValidatorFn {
        return (control: AbstractControl): ValidationErrorsType | null => {
            if(Validators.minLength(maxLength)(control) === null){
                return null
            }
            return {maxLength: {message: "Maksimal Karakter adalah " + maxLength + " Karakter"}}
        }
    }
    static noNumber(control: AbstractControl): ValidationErrorsType | null {
        if(!hasNumber.test(control.value)){
            return null;
        }
        return {noNumber: {message: "Tidak Boleh Menggunakan Angka !"}}
    }
    static numberOnly(control: AbstractControl): ValidationErrorsType | null {

        if(
            hasUpperCase.test(control.value) || 
            hasLowerCase.test(control.value) ||
            hasSpecialCharacter.test(control.value)
        ){

            return {numberOnly: {message: "Hanya Boleh Menggunakan Angka!"}};
        }
        return null;
    }
    static requiredField(field_name: string): ValidatorFn {
        return (control: AbstractControl): ValidationErrorsType | null => {
            if(Validators.required(control) !== null){
                return {required: {message: field_name + " Harus Diisi !"}}
            }
            return null;
        }
    }
}
