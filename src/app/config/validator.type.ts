import { NzSafeAny } from "ng-zorro-antd/core/types";

export type ValidationErrorsOptions = {message: string} & Record<string, NzSafeAny>;
export type ValidationErrorsType = Record<string, ValidationErrorsOptions>;