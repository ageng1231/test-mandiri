import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})
export class AppImage {
    public  LogoSrc = 'https://bankmandiri.co.id/image/layout_set_logo?img_id=31567&t=1718902960394';
}
