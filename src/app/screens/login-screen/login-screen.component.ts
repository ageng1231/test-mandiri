import { Component } from '@angular/core';
import { FormLoginComponent } from '../../shared/widgets/form-login/form-login.component';
import { NzCardModule } from 'ng-zorro-antd/card';

@Component({
  selector: 'app-login-screen',
  standalone: true,
  imports: [
    FormLoginComponent,
    NzCardModule
  ],
  templateUrl: './login-screen.component.html',
  styleUrl: './login-screen.component.css'
})
export class LoginScreenComponent {
  title = 'Login'
  ngOnInit(){

  }
}
