import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppLayoutModule } from '@mandiri/shared/layouts/app-layout/app-layout.module';
import { FormEmployeeComponent } from '@mandiri/shared/widgets/form-employee/form-employee.component';
import { ViewEmployeeComponent } from '@mandiri/shared/widgets/view-employee/view-employee.component';
import { NzCardModule } from 'ng-zorro-antd/card';

@Component({
  selector: 'app-form-employee-screen',
  
  standalone: true,
  imports: [
    FormEmployeeComponent,
    NzCardModule,
    AppLayoutModule,
    CommonModule,
    ViewEmployeeComponent
  ],
  templateUrl: './form-employee-screen.component.html',
  styleUrl: './form-employee-screen.component.css'
})
export class FormEmployeeScreenComponent implements OnInit {
    selectedId: any;
    editable: boolean = true;
    constructor(
      private router: Router
    ){

    }
    ngOnInit(){
      this.selectedId = window.history.state.id;
      console.warn(window.history.state)
      if(window.history.state.editable != false){
        this.editable = true;
      }else{
        this.editable = false;
      }
    }
}
