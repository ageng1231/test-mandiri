import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormEmployeeScreenComponent } from './form-employee-screen.component';

describe('FormEmployeeScreenComponent', () => {
  let component: FormEmployeeScreenComponent;
  let fixture: ComponentFixture<FormEmployeeScreenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormEmployeeScreenComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FormEmployeeScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
