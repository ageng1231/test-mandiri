import { Component } from '@angular/core';
import { TableEmployeeComponent } from '../../shared/widgets/table-employee/table-employee.component';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { AppLayoutModule } from '@mandiri/shared/layouts/app-layout/app-layout.module';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-employee-screen',
  standalone: true,
  imports: [
    TableEmployeeComponent,
    NzCardModule,
    NzTypographyModule,
    AppLayoutModule,
    CommonModule
  ],
  templateUrl: './employee-screen.component.html',
  styleUrl: './employee-screen.component.css'
})
export class EmployeeScreenComponent {

}
