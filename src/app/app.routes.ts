import { Routes } from '@angular/router';
import { LoginScreenComponent } from './screens/login-screen/login-screen.component';
import { EmployeeScreenComponent } from './screens/employee-screen/employee-screen.component';
import { FormEmployeeScreenComponent } from './screens/form-employee-screen/form-employee-screen.component';

export const routes: Routes = [
    {
        path: '', component: LoginScreenComponent
    },
    {
        path: 'employee',
        children: [
            {
                path: '', component: EmployeeScreenComponent
            },
            {
                path: 'form', component: FormEmployeeScreenComponent
            },
            {
                path: 'form/:id', component: FormEmployeeScreenComponent
            },
            {
                path: 'view/:id', component: FormEmployeeScreenComponent
            }
        ]
    }
];
