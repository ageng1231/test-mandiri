import { Injectable } from '@angular/core';
import { EmployeeEndpoint } from '../datasources/employee-endpoint';
import { AppConfig } from '@mandiri/config/app-config';
import { EmployeeEntity } from '@mandiri/infrastructures/core/entities/employee-entity';

export type supabaseEmployeeEntity = {
  employee_id?: string
  username: string
  first_name: string
  last_name: string
  email: string
  salary: number
  status: string
  group: string
  description: string
  birth: Date
  password: string
}

@Injectable({
  providedIn: 'root'
})
export class EmployeeServiceService {
  private endpoint: EmployeeEndpoint
  constructor(
    private appConfig: AppConfig
  ) { 
    this.endpoint = new EmployeeEndpoint(this.appConfig);
  }
  async getAllEmployeeData({
    page = 1,
    limit = 10,
    query = '',
    sort = []
  }): Promise<any> {
    let outData: EmployeeEntity[] = [];
    let range: [number,number];
    if(page == 1){
      range = [0, 10]
    }else{
      range = [(page * limit) - (limit ), (page * limit) - 1]
    }
    console.warn(range, query);
    let data = await this.endpoint.getAll(range,query, sort);
    console.warn(data);
    if(data.data.length > 0) {
      data.data.map((i:any, v:number) => {
        let row: EmployeeEntity = {
          employee_id: i.employee_id,
          username: i.username,
          firstName: i.first_name,
          lastName: i.last_name,
          email: i.email,
          basicSalary: i.salary,
          status: i.status,
          group: i.group,
          description: i.description,
          birthDate: i.birth
        }
        outData.push(row)

      })
    }
    return {data: outData, count: data.count}
  }
  
  private mapFromJson(json: any): EmployeeEntity {
    let row: EmployeeEntity = {
      employee_id: json.employee_id,
      username: json.username,
      firstName: json.first_name,
      lastName: json.last_name,
      email: json.email,
      basicSalary: json.salary,
      status: json.status,
      group: json.group,
      description: json.description,
      birthDate: json.birth
    }
    return row;
  }
  private mapToJson(data: EmployeeEntity): supabaseEmployeeEntity {
    let preparedData: supabaseEmployeeEntity = {
      username: data.username,
      email: data.email,
      birth: data.birthDate,
      first_name: data.firstName,
      last_name: data.lastName,
      salary: data.basicSalary,
      status: data.status,
      group: data.group,
      description: data.description,
      password: 'P@ssw0rd'
      
    }
    return preparedData;
  }
  async insertData(data: EmployeeEntity): Promise<any> {
    let preparedData = this.mapToJson(data);
    console.warn(preparedData)
    return await this.endpoint.create(preparedData);
  }
  async getData(id: string): Promise<any> {
    return await this.endpoint.get(id).then(r => this.mapFromJson(r.data));
  }
  async updateData(id: string, data: EmployeeEntity): Promise<any> {
    let preparedData = this.mapToJson(data);
    return await this.endpoint.update(id, preparedData);
  }
}
