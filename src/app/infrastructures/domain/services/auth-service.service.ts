import { Injectable } from '@angular/core';
import { AuthEndpoint } from '../datasources/auth-endpoint';

export type AuthRequestType = {
  email: string
  password: string
}

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  constructor(
    private endPoint: AuthEndpoint
  ) { }
  private mapToJson(data: any): AuthRequestType
  {
    let row: AuthRequestType = {
      email: data.username,
      password: data.password
    }
    return row
  }
  public async login(data: any): Promise<any>{
      let preparedData = this.mapToJson(data);
      return await this.endPoint.login(preparedData);
  }
}
