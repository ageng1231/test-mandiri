import { AppConfig } from "@mandiri/config/app-config";
import { SupabaseClient } from "@supabase/supabase-js";

export class EmployeeEndpoint {
    private dbClient: SupabaseClient;
    constructor(
        private appConfig: AppConfig
    ){
        this.dbClient = this.appConfig.getClient();
    }
    public async getAll(limit: [number, number] = [0,9], query: string = '', sort: any[] = []): Promise<any> {
        let preparedQuery = this.dbClient.from('employee').select('employee_id, username, first_name, last_name, salary, status, group, description, birth,email', {count: 'exact'});
        if(query != ''){
            preparedQuery = preparedQuery.or(`first_name.like.%`+query+`%,last_name.like.%`+query+`%, email.like.%`+query+ '')
  
        }
        if(sort.length > 0){
            let stringArr: string[] = [];
            sort.map((i: any, v: number) => {
                if(i.order == 'asc'){
                    stringArr.push(i.column);
                }
                
            })
            console.warn(stringArr, stringArr.join(', '))
            preparedQuery = preparedQuery.order(stringArr.join(', '));
        }
        preparedQuery = preparedQuery.range(limit[0], limit[1]);
        return await preparedQuery;
    }
    public async get(id: string): Promise<any> {
        return await this.dbClient.from('employee').select('employee_id, username, first_name, last_name, salary, status, group, description, birth,email').eq('employee_id', id).maybeSingle();
    }
    public async create(data: any): Promise<any>{
        return await this.dbClient.from('employee').insert(data);
    }
    public async update(id: string,data: any): Promise<any>{
        return await this.dbClient.from('employee').update(data).eq('employee_id', id);
    }
    public async delete(id: string): Promise<any>{
        return await this.dbClient.from('employee').delete().eq('employee_id', id);
    }
}
