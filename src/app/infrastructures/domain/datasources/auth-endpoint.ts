import { Injectable } from "@angular/core";
import { AppConfig } from "@mandiri/config/app-config";
import { SupabaseClient } from "@supabase/supabase-js";

@Injectable({
    providedIn: 'root'
})
export class AuthEndpoint {
    private dbClient: SupabaseClient;
    constructor(
        private appConfig: AppConfig
    ){
        this.dbClient = this.appConfig.getClient();
    }
    public async login(data: any): Promise<any> {
        return await this.dbClient.from('employee')
            .select('employee_id, username, first_name, last_name, salary, status, group, description, birth,email')
            .eq('email', data.email)
            .eq('password', data.password).limit(1).maybeSingle();
    }
    public async logout(data: any): Promise<any>{
        return await this.dbClient.from('employee').insert(data);
    }
    
}
