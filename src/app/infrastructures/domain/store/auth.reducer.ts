import { AuthActionsType, authAction, authActionType } from "./auth.actions";
import { Auth } from "./auth.model";

const initialState: Auth = {
    isLoggedIn: false,
    token: null    
}
export function AuthReducer(
    state: Auth = initialState,
    action: authActionType
){
    switch(action.type){
        case AuthActionsType.SAVE_TOKEN:
            return {
                ...state,
                token: action.payload
            }
        case AuthActionsType.SET_LOGIN:
            return {
                ...state,
                isLoggedIn: true
            }
        case AuthActionsType.LOGOUT:
            return {
                ...state,
                isLoggedIn: false,
                token: ''
            }
        default: 
            return state;
    }
}