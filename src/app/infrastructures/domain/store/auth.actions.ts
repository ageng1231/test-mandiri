import { Action } from "@ngrx/store";
import { Auth } from "./auth.model";
export enum AuthActionsType {
    SET_LOGIN = '[SET-LOGIN]',
    SAVE_TOKEN = '[SAVE-TOKEN]',
    LOGOUT = '[LOGOUT]',
}
export class authAction implements Action{
    constructor(
        public type: AuthActionsType,
        public payload: any
    ){}
}

export type authActionType = authAction;
